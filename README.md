# DTPD-Data processing

The data processing concerns the environment for the Data Analyst to manage the data flow after receipt from the Reporting Entity, that is: to validate data input, apply aggregations and validate data output before dissemination.    
The data output is to be based on an agreed dissemination data structure definition.    
The environment is to consist of a process space where the Data Analyst(s) can perform the needed tasks including aggregations, and a staging space for cleaned and validated data, and dashboard previews, and where decisions are made on the data to be disseminated in the final dashboard, and disseminate space.  
